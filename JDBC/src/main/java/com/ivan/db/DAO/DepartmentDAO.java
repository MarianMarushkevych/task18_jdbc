package com.ivan.db.DAO;


import com.ivan.db.model.DepartmentEntity;

public interface DepartmentDAO extends GeneralDAO<DepartmentEntity, String> {
}


